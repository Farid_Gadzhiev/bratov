from numpy import *
import numpy as nm
import matplotlib.pyplot as mpl

E = 2 * 10 ** 9
nu = 0.3
lyambda = nu * E / ((1.0 + nu) * (1.0 - 2.0 * nu))
mu = E / (2.0 * (1.0 + nu))
P = -300.0
D = [[E / (1.0 - nu ** 2), nu * E / (1.0 - nu ** 2), 0.0], [nu * E / (1.0 - nu ** 2), E / (1.0 - nu ** 2), 0.0],
     [0.0, 0.0, (1.0 - nu) / 2.0 * E / (1.0 - nu ** 2)]]
sigtech = 600

with open("n.txt", "r") as f, open("e.txt", "r") as w:
    nodes = []
    elements = []
    count_nodes = int(f.readline())
    count_elements = int(w.readline())

    for line in f:
        s = line.split("\t")
        s = list(map(lambda x: x.rstrip(), s))
        nodes.append([float(s[0]), float(s[1])])

    for line in w:
        s1 = line.split("\t")
        s1 = list(map(lambda x: x.rstrip(), s1))
        elements.append([int(s1[0]), int(s1[1]), int(s1[2])])


def sigma_yy(element, resh):
    # координаты узлов
    x_1, x_2, x_3 = nodes[element[0] - 1][0], nodes[element[1] - 1][0], nodes[element[2] - 1][0]
    y_1, y_2, y_3 = nodes[element[0] - 1][1], nodes[element[1] - 1][1], nodes[element[2] - 1][1]
    # Как изменится выражение для перемещений в координатах
    b_1, b_2, b_3 = y_2 - y_3, y_3 - y_1, y_1 - y_2
    c_1, c_2, c_3 = x_3 - x_2, x_1 - x_3, x_2 - x_1

    triangle_square = 0.5 * nm.linalg.det([[1.0, x_1, y_1], [1.0, x_2, y_2], [1.0, x_3, y_3]])  # ������� ������������

    dux = (resh[2 * (element[0] - 1)] * b_1 + resh[2 * (element[1] - 1)] * b_2 + resh[2 * (element[2] - 1)] * b_3) / (
            2.0 * triangle_square)  # компоненты перемещений
    dvy = (resh[2 * (element[0] - 1) + 1] * c_1 + resh[2 * (element[1] - 1) + 1] * c_2 + resh[
        2 * (element[2] - 1) + 1] * c_3) / (2.0 * triangle_square)

    return lyambda * (dux + dvy) + 2 * mu * dvy  # ���


# Правые части для локальных матриц жесткости (вектор) везде нулевые, кроме
# элементов с внешней нагрузкой
# Объемные силы?
def b_i(element):
    d = nm.zeros(6)
    x_1, x_2, x_3 = nodes[element[0] - 1][0], nodes[element[1] - 1][0], nodes[element[2] - 1][0]
    y_1, y_2, y_3 = nodes[element[0] - 1][1], nodes[element[1] - 1][1], nodes[element[2] - 1][1]
    a_1, a_2, a_3 = x_2 * y_3 - x_3 * y_2, x_3 * y_1 - x_1 * y_3, x_1 * y_2 - x_2 * y_1
    b_1, b_2, b_3 = y_2 - y_3, y_3 - y_1, y_1 - y_2
    c_1, c_2, c_3 = x_3 - x_2, x_1 - x_3, x_2 - x_1
    triangle_square = 0.5 * nm.linalg.det([[1.0, x_1, y_1], [1.0, x_2, y_2], [1.0, x_3, y_3]])
    a = [a_1, a_2, a_3]
    b = [b_1, b_2, b_3]
    c = [c_1, c_2, c_3]
    if nodes[element[0] - 1][1] == 10.0 and nodes[element[1] - 1][1] == 10.0:
        for h in range(3):
            d[2 * h + 1] = P * 1.0 / (2.0 * triangle_square) * (
                    (a[h] + 10.0 * c[h]) * (x_2 - x_1) + 0.5 * b[h] * (x_2 ** 2 - x_1 ** 2))
    elif nodes[element[1] - 1][1] == 10.0 and nodes[element[2] - 1][1] == 10.0:
        for h in range(3):
            d[2 * h + 1] = P * 1.0 / (2.0 * triangle_square) * (
                    (a[h] + 10.0 * c[h]) * (x_3 - x_2) + 0.5 * b[h] * (x_3 ** 2 - x_2 ** 2))

    elif nodes[element[0] - 1][1] == 10.0 and nodes[element[2] - 1][1] == 10.0:
        for h in range(3):
            d[2 * h + 1] = P * 1.0 / (2.0 * triangle_square) * (
                    (a[h] + 10.0 * c[h]) * (x_1 - x_3) + 0.5 * b[h] * (x_1 ** 2 - x_3 ** 2))
    return d


def K_i(element):
    x_1, x_2, x_3 = nodes[element[0] - 1][0], nodes[element[1] - 1][0], nodes[element[2] - 1][0]
    y_1, y_2, y_3 = nodes[element[0] - 1][1], nodes[element[1] - 1][1], nodes[element[2] - 1][1]
    # a_1, a_2, a_3 = x_2 * y_3 - x_3 * y_2, x_3 * y_1 - x_1 * y_3, x_1 * y_2 - x_2 * y_1
    b_1, b_2, b_3 = y_2 - y_3, y_3 - y_1, y_1 - y_2
    c_1, c_2, c_3 = x_3 - x_2, x_1 - x_3, x_2 - x_1
    F = 0.5 * nm.linalg.det([[1.0, x_1, y_1], [1.0, x_2, y_2], [1.0, x_3, y_3]])
    B = [[b_1 / (2.0 * F), 0.0, b_2 / (2.0 * F), 0.0, b_3 / (2.0 * F), 0.0],
         [0.0, c_1 / (2.0 * F), 0.0, c_2 / (2.0 * F), 0.0, c_3 / (2.0 * F)],
         [c_1 / (2.0 * F), b_1 / (2.0 * F), c_2 / (2.0 * F), b_2 / (2.0 * F), c_3 / (2.0 * F), b_3 / (2.0 * F)]]
    K = nm.dot(nm.dot(nm.transpose(B), D), B) * F
    return K


def getBb():
    bb = nm.zeros(2 * count_nodes, "f")
    for element in elements:
        bbb = b_i(element)
        for i in range(6):
            if i % 2 == 0:
                bb[2 * (element[i // 2] - 1)] += bbb[i]
            else:
                bb[2 * (element[i // 2] - 1) + 1] += bbb[i]
    return bb


def getKf(bb):
    x_0 = []
    y_0 = []
    K_f = nm.zeros((2 * count_nodes, 2 * count_nodes), "f")  # ����������
    for element in elements:
        Ki = K_i(element)

        for i in range(6):
            for j in range(6):
                if i % 2 == 0 and j % 2 == 0:
                    K_f[2 * (element[i // 2] - 1)][2 * (element[j // 2] - 1)] += Ki[i][j]
                if i % 2 == 0 and j % 2 != 0:
                    K_f[2 * (element[i // 2] - 1)][2 * (element[j // 2] - 1) + 1] += Ki[i][j]
                if i % 2 != 0 and j % 2 == 0:
                    K_f[2 * (element[i // 2] - 1) + 1][2 * (element[j // 2] - 1)] += Ki[i][j]
                if i % 2 != 0 and j % 2 != 0:
                    K_f[2 * (element[i // 2] - 1) + 1][2 * (element[j // 2] - 1) + 1] += Ki[i][j]

    print("Global matrix was built")

    v = 1
    for i in nodes:
        if i[0] == 0.0:
            x_0.append(v)
        if i[1] == 0.0:
            y_0.append(v)
        v += 1
    for i in y_0:
        for element in range(2 * count_nodes):
            K_f[2 * (i - 1) + 1][element] = 0.0
            K_f[element][2 * (i - 1) + 1] = 0.0
        K_f[2 * (i - 1) + 1][2 * (i - 1) + 1] = 1.0
        bb[2 * (i - 1) + 1] = 0.0
    for i in x_0:
        for element in range(2 * count_nodes):
            K_f[2 * (i - 1)][element] = 0.0
            K_f[element][2 * (i - 1)] = 0.0
        K_f[2 * (i - 1)][2 * (i - 1)] = 1.0
        bb[2 * (i - 1)] = 0.0
    return K_f


def resh_resh(x_0, y_0):
    bb = nm.zeros(2 * count_nodes, "f")
    K_f = getKf(bb)
    resh = nm.linalg.solve(K_f, bb)
    print("System solved")
    return resh


def solve_system(x_0, y_0, glob_s, min_list, max_list):
    resh = resh_resh(x_0, y_0)
    s = []
    for element in elements:
        s.append(sigma_yy(element, resh))
    glob_s.append(s)
    min_list.append(nm.min(s))
    max_list.append(nm.max(s))


def static():
    x_0 = []
    y_0 = []
    glob_s = []
    min_list = []
    max_list = []
    solve_system(x_0, y_0, glob_s, min_list, max_list)
    minValue = nm.min(min_list)
    maxValue = nm.max(max_list)

    fig = mpl.figure(figsize=(10, 10))
    ax = fig.add_subplot(111)
    k = 0
    l = (maxValue - minValue) / 9.0
    col = ['#0000FF', '#008B8B', '#00BFFF', '#00FFF0', '#7FFFD0', '#7FFF00', '#FFD700', '#DAA520', '#FF0000']
    for i in elements:
        for j in range(9):
            if (minValue + j * l) <= glob_s[0][k] < (minValue + (j + 1) * l):
                ax.fill([nodes[i[0] - 1][0], nodes[i[1] - 1][0],
                         nodes[i[2] - 1][0]],
                        [nodes[i[0] - 1][1], nodes[i[1] - 1][1],
                         nodes[i[2] - 1][1]], col[j], linewidth=0.001)
        k += 1
    ax.fill([0, 10, 10, 0], [0, 0, -2, -2], 'w')
    for i in range(9):
        ax.fill([0.1 + i, 0.1 + i + 1, 0.1 + i + 1, 0.1 + i], [-1, -1, -0.5, -0.5], col[i])
        if i % 2 == 0:
            ax.text(0.1 + i, -1.5, str(round(minValue + l * i, 2)))
        else:
            ax.text(0.1 + i, -1.9, str(round(minValue + l * i, 2)))

    ax.text(0.1 + 9, -1.9, str(round(minValue + l * 9, 2)))
    ax.text(5.0, 5.0, "P=" + str(P))
    mpl.savefig(str("result1"))


def criteria(sig1, sig2):
    return (sig1 ** 2 + sig2 ** 2 - sig1 * sig2) ** 0.5 - sigtech


def plastic():
    global elemtech
    bb = getBb()
    stiffnessMatrix = getKf(bb)
    C = [[0] * 3 for i in range(3)]
    lastsolution = zeros((count_nodes, 2))
    elemtech = []

    spnum = 0
    sp = zeros((3 * count_elements, 6))
    for elementNumber in range(count_elements):
        element = elements[elementNumber]
        for i in range(0, 3):
            node = int(element[i]) - 1
            C[i][0] = 1
            C[i][1] = float(nodes[node][0])
            C[i][2] = float(nodes[node][1])

        Nijmx = array([0, 1, 0]) @ linalg.inv(C)
        Nijmy = array([0, 0, 1]) @ linalg.inv(C)
        t1 = 0
        for t in range(0, 5, 2):
            sp[3 * spnum][t] = Nijmx[t1]
            sp[3 * spnum + 2][t] = Nijmy[t1]
            sp[3 * spnum + 1][t + 1] = Nijmy[t1]
            sp[3 * spnum + 2][t + 1] = Nijmx[t1]
            t1 += 1

        spnum += 1

    solution = linalg.inv(stiffnessMatrix) @ bb

    for line in range(count_elements):
        localdispace = zeros((6, 1))
        element = elements[line]
        for i in range(3):
            node = int(element[i]) - 1
            localdispace[2 * i] = solution[2 * node]
            localdispace[2 * i + 1] = solution[2 * node + 1]
        sigma = nm.dot(nm.dot(D, sp[3 * line:3 * line + 3][0:6]), localdispace)
        sigma1 = (sigma[0, 0] + sigma[1, 0]) / 2 + 1 / 2 * (
                (sigma[0, 0] - sigma[1, 0]) ** 2 + 4 * (sigma[2, 0]) ** 2) ** 0.5
        sigma2 = (sigma[0, 0] + sigma[1, 0]) / 2 - 1 / 2 * (
                (sigma[0, 0] - sigma[1, 0]) ** 2 + 4 * (sigma[2, 0]) ** 2) ** 0.5
        sigmaMises = (sigma1 ** 2 + sigma2 ** 2 - sigma1 * sigma2) ** 0.5
        sigmaTemp1 = sigma1
        sigmaTemp2 = sigma2

        if sigmaMises > sigtech:
            print('Yield function: ', criteria(sigma1, sigma2), )
            while criteria(sigma1, sigma2) > 10 ** -5:
                d11 = (2 * sigma1 - sigma2) / 2 / (
                        (sigma1 ** 2 + sigma2 ** 2 - sigma1 * sigma2) ** 0.5)  # по 1гл. напр.
                d22 = (2 * sigma2 - sigma1) / 2 / (
                        (sigma1 ** 2 + sigma2 ** 2 - sigma1 * sigma2) ** 0.5)  # по 2гл. напр.
                dfunc = array([d11, d22, 0])
                DD = array([[1, nu, 0], [nu, 1, 0], [0, 0, (1 - nu) / 2]])
                denominator = transpose(dfunc) @ DD @ dfunc
                lamb = criteria(sigma1, sigma2) / denominator
                sigplast = lamb * DD @ dfunc
                sigma1 += -sigplast[0]
                sigma2 += -sigplast[1]
            sigmaPlastic = ((sigmaTemp1 - sigma1) ** 2 + (sigmaTemp2 - sigma2) ** 2) ** 0.5
            print('Plastic deformation: ', sigmaPlastic)
            elemtech.append(line)  # список элементов где началась текучесть
            print('Plastic element: ', line)
            print("Criteria: ", criteria(sigma1, sigma2))
            print()
    for node in range(count_nodes):
        lastsolution[node][0] = nodes[node][0] + solution[2 * node]
        lastsolution[node][1] = nodes[node][1] + solution[2 * node + 1]
    # print(lastsolution)
    print(elemtech)


def main():
    static()


if __name__ == "__main__":
    main()
