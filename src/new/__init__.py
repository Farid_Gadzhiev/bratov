from numpy import *
import numpy as np

youngModulus = 2 * 10 ** 9  # модуль упругости
puasson = 0.3  # коэффициент Пуассона
tol = 2
sigtech = 300

elementsFile = open('e_5.txt', "r")
elementsLines = elementsFile.readlines()
countElements = len(elementsLines)

nodesFile = open('n_5.txt', "r")
nodesLines = nodesFile.readlines()
countNodes = len(nodesLines)
nodes = zeros((countNodes, 2))

for i in range(countNodes):
    temp = nodesLines[i].split()
    nodes[i][0] = float(temp[1])
    nodes[i][1] = float(temp[2])

C = [[0] * 3 for i in range(3)]  # создание 0-й матрицы C 3*3
B = [[0] * 6 for i in range(3)]  # создание 0-й матрицы B 3*6
D = youngModulus / (1 - puasson ** 2) * array([[1, puasson, 0], [puasson, 1, 0], [0, 0, (1 - puasson) / 2]])
matsig = zeros((countElements, 4))
lastsolution = zeros((countNodes, 2))
lastmatsig = zeros((countElements, 4))


def func(sig1, sig2):
    func1 = (sig1 ** 2 + sig2 ** 2 - sig1 * sig2) ** 0.5 - sigtech
    return func1


# print(D)
for vprir in range(11, 30, 2):
    spnum = 0
    elemtech = []
    sp = zeros((3 * countElements, 6))
    stiffnessMatrix = zeros((2 * countNodes, 2 * countNodes))  # ОБЩАЯ матрица жесткости

    for elementNumber in range(countElements):
        element = elementsLines[elementNumber].split()
        a = [[0] * 3 for j in range(4)]
        for i in range(0, 3):
            nu = int(element[i]) - 1
            C[i][0] = 1
            C[i][1] = float(nodes[nu][0])
            C[i][2] = float(nodes[nu][1])

        Nijmx = array([0, 1, 0]) @ linalg.inv(C)
        Nijmy = array([0, 0, 1]) @ linalg.inv(C)
        s = abs(linalg.det(C) / 2)
        t1 = 0
        for t in range(0, 5, 2):
            rb = 0
            B[rb][t] = Nijmx[t1]
            B[rb + 2][t] = Nijmy[t1]
            sp[3 * spnum][t] = Nijmx[t1]
            sp[3 * spnum + 2][t] = Nijmy[t1]
            rb = 1
            B[rb][t + 1] = Nijmy[t1]
            B[rb + 1][t + 1] = Nijmx[t1]
            sp[3 * spnum + 1][t + 1] = Nijmy[t1]
            sp[3 * spnum + 2][t + 1] = Nijmx[t1]
            t1 += 1

        ke = np.transpose(B) @ D @ B * s * tol  # Ki

        for i in range(3):
            node = int(element[i])
            x1 = (node - 1) * 2
            x2 = (node - 1) * 2 + 1
            for j in range(3):
                h = int(element[j])

                y1 = (h - 1) * 2
                y2 = (h - 1) * 2 + 1
                stiffnessMatrix[x1][y1] += ke[i * 2][j * 2]
                stiffnessMatrix[x1][y2] += ke[i * 2][j * 2 + 1]
                stiffnessMatrix[x2][y1] += ke[i * 2 + 1][j * 2]
                stiffnessMatrix[x2][y2] += ke[i * 2 + 1][j * 2 + 1]
        spnum += 1

    load = open('SFLIS1.txt', 'r')  # файл с нагрузками
    v = zeros((2 * countNodes, 1))
    for line in load.readlines():
        ld = line.split()
        ld1 = (int(ld[0]) - 1) * 2 + 1
        v[ld1] = 100 + vprir / 2
    v[(20 - 1) * 2 + 1] = 50 + vprir / 2 / 2
    v[(40 - 1) * 2 + 1] = 50 + vprir / 2 / 2
    # print(v)
    # print(m1,'etrytuvyk')
    # файл с ограничениями перемещений
    for line1 in reversed(open('DLIST.txt').readlines()):  # чтение с конца 39 файла
        ert = line1.split()
        tyu = int(ert[0])
        uxdel = (tyu - 1) * 2  # номер строки и столбца для удаления
        uydel = (tyu - 1) * 2 + 1  # номер строки и столбца для удаления
        # print(ert[1])
        if ert[1] == 'UX':
            stiffnessMatrix = delete(stiffnessMatrix, uxdel, 0)
            stiffnessMatrix = delete(stiffnessMatrix, uxdel, 1)
            v = delete(v, uxdel, 0)
        else:
            stiffnessMatrix = delete(stiffnessMatrix, uydel, 0)
            stiffnessMatrix = delete(stiffnessMatrix, uydel, 1)
            v = delete(v, uydel, 0)

    # print(v)
    # print(m1,'etrytuvyk')
    # solution=linalg.solve(m1,v)
    solution = linalg.inv(stiffnessMatrix) @ v
    # print(solution)

    for db in open('DLIST.txt').readlines():
        jj = db.split()
        if jj[1] == 'UX':
            solution = insert(solution, 2 * (int(jj[0]) - 1), 0)
        else:
            solution = insert(solution, 2 * (int(jj[0]) - 1) + 1, 0)

    ft = open(r'result.txt', 'w')
    for soli in range(len(solution)):
        strsol = solution[soli]
        ft.write(str(strsol) + '\n')
    ft.close()



    for linest in range(countElements):
        localdispace = zeros((6, 1))
        w1st = elementsLines[linest].split()
        for i in range(3):
            nust = int(w1st[i]) - 1
            localdispace[2 * i] = solution[2 * nust]
            localdispace[2 * i + 1] = solution[2 * nust + 1]
        sigma = D @ sp[3 * linest:3 * linest + 3][0:6] @ localdispace
        sigmamisis = ((sigma[0, 0]) ** 2 - (sigma[0, 0]) * (sigma[1, 0]) + (sigma[1, 0]) ** 2 + 3 * (
            sigma[2, 0]) ** 2) ** 0.5
        # print(sigmamisis)
        sig1 = (sigma[0, 0] + sigma[1, 0]) / 2 + 1 / 2 * (
                (sigma[0, 0] - sigma[1, 0]) ** 2 + 4 * (sigma[2, 0]) ** 2) ** 0.5
        sig2 = (sigma[0, 0] + sigma[1, 0]) / 2 - 1 / 2 * (
                (sigma[0, 0] - sigma[1, 0]) ** 2 + 4 * (sigma[2, 0]) ** 2) ** 0.5
        sigmamisis2 = (sig1 ** 2 + sig2 ** 2 - sig1 * sig2) ** 0.5
        sig1b = sig1
        sig2b = sig2

        if sigmamisis2 > sigtech:
            print(func(sig1, sig2), 'функция f')
            while func(sig1, sig2) > 10 ** -5:
                d11 = (2 * sig1 - sig2) / 2 / ((sig1 ** 2 + sig2 ** 2 - sig1 * sig2) ** 0.5)  # по 1гл. напр.
                d22 = (2 * sig2 - sig1) / 2 / ((sig1 ** 2 + sig2 ** 2 - sig1 * sig2) ** 0.5)  # по 2гл. напр.
                dfunc = array([d11, d22, 0])
                DD = array([[1, puasson, 0], [puasson, 1, 0], [0, 0, (1 - puasson) / 2]])
                znam = transpose(dfunc) @ DD @ dfunc
                lamb = func(sig1, sig2) / znam
                sigplast = lamb * DD @ dfunc
                sig1 += -sigplast[0]
                sig2 += -sigplast[1]
                # print(sigplast)
            sigplastic = ((sig1b - sig1) ** 2 + (sig2b - sig2) ** 2) ** 0.5
            print(sigplastic, 'общая пласт. деф')
            # print(func(sig1,sig2),'измен. f')
            elemtech.append(linest)  # список элементов где началась текучесть
            prev = sigmamisis - sigtech
            # print(prev,'разница')
            print(linest, 'Plastic element')
            matsig[linest][0] = sigma[0]
            matsig[linest][1] = sigma[1]
            matsig[linest][2] = sigma[2]
            matsig[linest][3] = sigmamisis
            print(func(sig1, sig2))
    for elem1 in range(countNodes):
        lastsolution[elem1][0] = nodes[elem1][0] + solution[2 * elem1]
        lastsolution[elem1][1] = nodes[elem1][1] + solution[2 * elem1 + 1]

print(elemtech)
